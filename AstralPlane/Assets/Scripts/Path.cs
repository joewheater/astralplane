﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Path", menuName = "Path")]
public class Path : ScriptableObject
{
    [SerializeField] private int _minPathLength;
    public int MinPathLength {
        get { return _minPathLength; }
    }

    [SerializeField] private int _maxPathLength;
    public int MaxPathLength {
        get { return _maxPathLength; }
    }

    [SerializeField] private Vector3 _startPoint;
    public Vector3 StartPoint
    {
        get { return _startPoint; }
    }

    [SerializeField] private Vector3 _endPoint;
    public Vector3 EndPoint
    {
        get { return _endPoint; }
    }

    [SerializeField] private Enums.Direction _startDirection;
    public Enums.Direction StartDirection
    {
        get { return _startDirection; }
    }
}

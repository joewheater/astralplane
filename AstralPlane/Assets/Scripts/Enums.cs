﻿
/// <summary>
/// Class to contain enums
/// </summary>
public class Enums {

    public enum Direction
    {
        None = 0x00,
        Left = 0x01,
        Forward = 0x02,
        Right = 0x03,
        Back = 0x04,
        Up = 0x05,
        Down = 0x06
    }
}

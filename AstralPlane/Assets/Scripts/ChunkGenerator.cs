﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class ChunkGenerator : MonoBehaviour
{
    public Texture2D image;

    public GameObject blockPrefab;

    private const float c_blockScale = 1;

    void Start()
    {
        GenerateChunkFromImage(image);
    }

    private void GenerateChunkFromImage(Texture2D inImage)
    {
        int width = inImage.width;
        int height = inImage.height;

        Color[] pixelArray = image.GetPixels(0, 0, width, height);

        int i = 0;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color pixelColor = pixelArray[i];

                if (pixelColor.a > 0.5f)
                {
                    float xPos = x - (width / 2) + (c_blockScale / 2f);
                    float yPos = y - (height / 2) + (c_blockScale / 2f);
                    Instantiate(blockPrefab, new Vector3(xPos, 0, yPos), Quaternion.identity);
                }

                i++;
            }
        }
    }
}

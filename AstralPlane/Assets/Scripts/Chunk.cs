﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : MonoBehaviour
{
    public ChunkData chunkData;

    public byte[,,] chunkMap; // map of blocks in chunk

    private int _chunkWidth;
    private int _chunkHeight;
    private int _chunkDepth;

    public void Create(byte inWidth, byte inHeight, byte inDepth)
    {
        // Store chunk dimensions
        _chunkWidth = inWidth;
        _chunkHeight = inHeight;
        _chunkDepth = inDepth;

        // Generate chunk block map
        chunkMap = new byte[inWidth, inHeight, inDepth];
    }

}
